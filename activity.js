

// name - single
// accomodates - 2
// price - 1000
// description - A simple room with all the basic necessities
// rooms_available - 10
// isAvailable - false

db.Rooms.insertOne({

	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms available": "10",
	"isActive": false,
})

// Insert multiple rooms (insertMany method) with the following
// details:
// a. name - double
// b. accomodates - 3
// c. price - 2000
// d. description - A room fit for a small family going on a vacation
// e. rooms_available - 5
// f. isAvailable - false

db.Rooms.insertMany([
	{
		"name": "queen",
		"accomodates": 4,
		"price": 4000,
		"description": "A room with a queen sized bed perfect for a simple getaway",
		"rooms available": "15",
		"isAvailable": false,
	}
])

db.Rooms.updateOne(
		{"name": "queen"},
		{
			$set: {
				"name": "queen",
				"accomodates": 4,
				"price": 4000,
				"description": "A room with a queen sized bed perfect for a simple getaway",
				"rooms available": "0",
				"isAvailable": false,
				
			}
		}

)

db.Rooms.deleteMany({
	"rooms available": "0",
	
})







